const {BaseRepository} = require('./baseRepository');

class AuthRepository extends BaseRepository{
    constructor() {
        super('users');
    }
    getOne(search) {
        return super.getOne(search);
    }
}

exports.authRepository = new AuthRepository();