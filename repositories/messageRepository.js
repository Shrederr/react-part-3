const {BaseRepository} = require('./baseRepository');

class MessageRepository extends BaseRepository {
    constructor() {
        super('message');
    }

    create(data) {
        return super.create(data);
    }

    getOne(search) {
        return super.getOne(search);
    }

    update(id, dataToUpdate) {
        return super.update(id, dataToUpdate);
    }

    delete(id) {
        return super.delete(id);
    }
}

exports.messageRepository = new MessageRepository();