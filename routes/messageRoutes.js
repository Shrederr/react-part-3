const {Router} = require('express');
const messagesService = require('../services/messageService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/', async (req, res, next) => {
    try {
        await messagesService.createMessage(req.body);
        res.data = messagesService.getAllMessage();
        next();
    } catch (error) {
        res.status(400).send({
            error: true,
            message: "error.message"
        });
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        res.data = messagesService.getAllMessage();
        next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.put('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        messagesService.editMessageText(id, req.body);
        res.data = messagesService.getAllMessage();
        next();
    } catch (error) {
        res.status(400).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.delete(`/:id`, (req, res, next) => {
    try {
        const id = req.params.id;
        messagesService.deleteMessage(id);
        res.data = messagesService.getAllMessage();
            next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);


module.exports = router;