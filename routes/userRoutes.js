const {Router} = require('express');
const userService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.post('/', async (req, res, next) => {
    try {
        await userService.createUser(req.body);
        res.data = userService.getAllUsers();
        next();
    } catch (error) {
        res.status(400).send({
            error: true,
            message: "error.message"
        });
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        res.data = userService.getAllUsers();
        next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.get(`/:id`, (req, res, next) => {
    try {
        const id = req.params.id;
        res.data = userService.getOneUser(id);
        next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.put(`/:id`, updateUserValid, (req, res, next) => {
    try {
        const id = req.params.id;
        userService.updateUserInfo(id, req.body);
        res.data = userService.getAllUsers();
        next();
    } catch (error) {
        res.status(400).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

router.delete(`/:id`, (req, res, next) => {
    try {
        const id = req.params.id;
        userService.deleteUser(id);
        res.data = userService.getAllUsers();
        next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);


module.exports = router;