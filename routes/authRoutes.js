const {Router} = require('express');
const AuthService = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        res.data = AuthService.login(req.body);
        next();
    } catch (error) {
        res.status(404).send({
            error: true,
            message: error.message
        });
    }
}, responseMiddleware);

module.exports = router;