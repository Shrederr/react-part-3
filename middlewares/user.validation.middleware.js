const {user} = require('../models/user');

const createUserValid = (req, res, next) => {
    delete user.id;
    const arrayOfValidation = Object.keys(user);
    const candidateForCreate = req.body;
    const arrayOfRequestKeys = Object.keys(candidateForCreate);
    if (candidateForCreate.id) {
        res.status(400).send({
            error: true,
            message: `id forbidden field in the request body`
        });
    }
    requestValid(arrayOfValidation, arrayOfRequestKeys, 'You need to fill in the field', res);
    checkPasswordForValid(candidateForCreate.password, 'Password is no correct', res);

    next();
}

const updateUserValid = (req, res, next) => {
    delete user.id;
    const arrayOfValidation = Object.keys(user);
    const candidateForUpdate = req.body;
    const arrayOfRequestKeys = Object.keys(candidateForUpdate);
    requestValid(arrayOfRequestKeys, arrayOfValidation, 'Invalid field in request body', res);
    next();
}

const requestValid = function (arrayOfValid, arrayToValidation, message, res) {
    arrayOfValid.forEach((field) => {
        if (!arrayToValidation.includes(field)) {
            res.status(400).send({
                error: true,
                message: message + " " + field
            });
        }
    });
}



const checkPasswordForValid = function (password, message, res) {
    if (!/^[A-Za-z0-9][A-Za-z0-9.]{3}/.test(password)) {
        res.status(400).send({
            error: true,
            message: message
        });
    }
}



exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;