const {messageRepository} = require('../repositories/messageRepository');

class messagesService {

    async createMessage(message) {
        return messageRepository.create(message);
    }

    getAllMessage() {
        return messageRepository.getAll(messageRepository.constructor);
    }

    editMessageText(id, dataToUpdate) {
        const candidateForUpdate = isExist(id, "There is no such fighter");
        const arrayOfCandidatesKeys = Object.keys(dataToUpdate);
        arrayOfCandidatesKeys.forEach((field) => {
            if (dataToUpdate[field] === candidateForUpdate[field]) {
                delete dataToUpdate[field];
            }
        });

        return messageRepository.update(id, dataToUpdate);
    }

    deleteMessage(id) {
        const candidateForDelete = isExist(id, "There is no such fighter");
        messageRepository.delete(id);
        return candidateForDelete;
    }
}

const isExist = function (id, message) {
    const isExist = messageRepository.getOne({id});
    if (!isExist) {
        throw new Error(
            message
        );
    }
    return isExist;
}

const alreadyExist = function (parameter, message) {
    const alreadyExist = messageRepository.getOne(parameter);
    if (alreadyExist) {
        throw new Error(
            message
        );
    }
}

module.exports = new messagesService ();