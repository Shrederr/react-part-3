const {UserRepository} = require('../repositories/userRepository');

class UserService {

    async createUser(user) {
        return UserRepository.create(user);
    }

    getAllUsers() {
        return UserRepository.getAll(UserRepository.constructor);
    }

    getOneUser(id) {
        return isExist(id, "Such user does not exist");
    }

    updateUserInfo(id, dataToUpdate) {
        const candidateForUpdate = isExist(id, "Such user does not exist");
        const arrayOfCandidatesKeys = Object.keys(dataToUpdate);
        arrayOfCandidatesKeys.forEach((field) => {
            if (dataToUpdate[field] === candidateForUpdate[field]) {
                delete dataToUpdate[field];
            }
        });
        return UserRepository.update(id, dataToUpdate);
    }

    deleteUser(id) {
        const candidateForDelete = isExist(id, 'This user does not exist')
        UserRepository.delete(id);
        return candidateForDelete;
    }
}

const isExist = function (id, message) {
    const isExist = UserRepository.getOne({id});
    if (!isExist) {
        throw new Error(
            message
        );
    }
    return isExist;
}

module.exports = new UserService();