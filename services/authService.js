const {authRepository} = require('../repositories/authRepository');

class AuthService {
    login(userData) {

        const user = authRepository.getOne({username:userData.username});
        if (!user) {
            throw new Error('User not found');
        }
        if (userData.password !== user.password) {
            throw new Error('Password is not correct');
        }
        return user;
    }
}

module.exports = new AuthService();