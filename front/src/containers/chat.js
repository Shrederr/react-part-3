import React, {useEffect, useState} from 'react';
import Header from "./header";
import MessageList from "./message-list";
import MessageInput from "./input";
import {useDispatch, useSelector} from "react-redux"
import * as ChatActions from "../store/ChatActions"
import * as userListActions from "../store/userListAction"
import uuid from 'react-uuid';
import LoadingSpinner from "../components/spiner";
import {useHistory} from "react-router-dom";

const Chat = () => {
    const chat = useSelector(state => state.chat);
    const app = useSelector(state => state.app);
    const dispatch = useDispatch();
    let history = useHistory();
    const [currentUser] = useState({
        chatName: `chat of ${app.user.username}`,
        currentUserId: app.user.id,
        currentUserName: app.user.username,
        currentUserAvatar: app.user.avatar
    })


    function sortByDate(arr) {
        arr.sort((a, b) => new Date(a.createdAt).getTime() > new Date(b.createdAt).getTime() ? 1 : -1);
    }

    function likeMessage(id) {
        dispatch(ChatActions.likeMessage(id))
    }

    function deleteMessage(id) {
        dispatch(ChatActions.deleteMessage(id))
    }

    function openEditor(id) {
        dispatch(ChatActions.switchEditor({
            isOpen: true,
            id: id
        }));
        history.push('/editor');
    }

    function sendMessage() {
        const date = new Date().toISOString();
        const newMessage = {
            id: uuid(),
            userId: currentUser.currentUserId,
            avatar: currentUser.currentUserAvatar,
            text: chat.message,
            user: currentUser.currentUserName,
            createdAt: date,
            editedAt: ""
        }
        if (!chat.message.replace(/\s+/g, '')) {
            return;
        }
        dispatch(ChatActions.sendMessage(newMessage));
    }

    function changeMessage(e) {
        dispatch(ChatActions.setMessage(e.target.value));
    }

    useEffect(() => {
        if (!app.isAuth) {
            history.push('/');
        }
        dispatch(userListActions.getUsersList());
        dispatch(ChatActions.getMessages());
        sortByDate(chat.messageList);

    }, []);


    if (!chat.spinner) {
        return (

            <div>
                <Header
                    chatName={currentUser.chatName}
                    lastMessageDate={chat.lastMessageDate}
                />
                <MessageList messageList={chat.messageList}
                             currentUserId={currentUser.currentUserId}
                             likeMessage={likeMessage}
                             likeList={chat.likes}
                             deleteMessage={deleteMessage}
                             editMessage={openEditor}
                />
                <MessageInput sendMessage={sendMessage} changeMessage={changeMessage} message={chat.message}/>
            </div>
        )
    } else {
        return (
            <div>
                <LoadingSpinner/>
            </div>
        )
    }
}


export default (Chat);