import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux"
import {useHistory} from "react-router-dom";
import * as userListActions from "../../store/userListAction";
import LoadingSpinner from "../../components/spiner";
import "../../styles/userList.css";

const UserList = () => {

    const userList = useSelector(state => state.userList);
    const app = useSelector(state => state.app);
    const dispatch = useDispatch();
    let history = useHistory();

    function addUserButton() {
        history.push('/user');
    }

    function goToChat() {
        history.push('/chat');
    }

    function editButton(id) {
        history.push(`/user/${id}`)
    }

    function deleteUser(id) {
        dispatch(userListActions.deleteUser(id));
    }

    useEffect(() => {
        if (!app.isAuth) {
            history.push('/');
        }
        dispatch(userListActions.getUsersList());

    }, []);
    if (!userList.spinner) {
        return (
            <div className="User-list-wrapper">
                <input type="button" className="Add-user-button" value="Add user" onClick={addUserButton}/>
                <input type="button" className="GoToChatButton" value="Go to chat" onClick={goToChat}/>
                <div className="User-list">
                    {
                        userList.userList.map((user) => {
                            return <div className="user-block" key={user.id}>
                                <div className="username-block">{user.username}</div>
                                <input type="button" value="edit" onClick={editButton.bind(null, user.id)}/>
                                <input type="button" value="delete" onClick={deleteUser.bind(null, user.id)}/>
                            </div>
                        })
                    }
                </div>
            </div>
        )
    } else {
        return (
            <div>
                <LoadingSpinner/>
            </div>
        )
    }
};


export default (UserList);