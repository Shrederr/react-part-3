import "../styles/header.css";
import {useSelector} from "react-redux";
import {useHistory} from "react-router-dom";


const Header = ({chatName, lastMessageDate}) => {
    const chat = useSelector(state => state.chat);
    const userList = useSelector(state => state.userList);
    const app = useSelector(state => state.app);
    let history = useHistory();

    function goToUserList() {
        if (app.user.admin) {
            history.push('/user-list');
        }
    }

    return (
        <div className="chat_header">
            <div className="chat_info">
                <div className="chatName headerBlock" onClick={goToUserList}>{chatName}</div>
                <div className="users_counts headerBlock">{userList.userCounts + " " + "participants"}</div>
                <div className="messages_counts headerBlock">{chat.messageCounts + " " + "messages"}</div>
                <div
                    className="last_message_date headerBlock">{"last message at" + " " + new Date(lastMessageDate).toDateString()}</div>
            </div>
        </div>
    )
}

export default Header;