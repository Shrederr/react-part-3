import React from "react";
import "../styles/input-message.css";


export default class MessageInput extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="user-interface">
                <input className="message-fill"
                       id="textData"
                       type="text"
                       value={this.props.message}
                       onChange={this.props.changeMessage}
                />
                <div className="button-wrapper">
                    <button className="send-button" id="sendData" onClick={this.props.sendMessage}>Send</button>
                </div>
            </div>
        )
    }
}
