import React, {useEffect, useState} from "react";

import "../../styles/editor.css";
import {useDispatch, useSelector} from "react-redux";
import * as ChatActions from "../../store/ChatActions";
import {useHistory} from "react-router-dom";

const EditModal = () => {

    const chat = useSelector(state => state.chat);
    const app = useSelector(state => state.app);
    const [newEditMessage, setNewEditMessage] = useState("");
    const dispatch = useDispatch()
    let history = useHistory();

    function closeEditor() {
        dispatch(ChatActions.switchEditor({
            isOpen: false,
            id: ""
        }));
        history.push('/chat');
    }

    function sendEditMessage() {
        dispatch(ChatActions.sendEditMessage(newEditMessage, chat.mutableMessage.id));
        closeEditor();
    }

    function changeNewMessage(e) {
        setNewEditMessage(e.target.value);
    }

    let classList = "editor-wrapper closeEditor";
    useEffect(() => {
        if (!app.isAuth) {
            history.push('/');
        }
    }, []);

    return (
        <div>
            <div className={classList}>
                Messages Editor
                <textarea
                    className="message-fill"
                    id="textEditData"
                    value={newEditMessage}
                    onChange={changeNewMessage}
                />
                <div>
                    <input type="button" id="sendEditMessage" onClick={sendEditMessage} value="Ok"/>
                    <input type="button" id="closeEditWindow" value="Cancel" onClick={closeEditor}/>
                </div>
            </div>

        </div>
    )
};
export default EditModal;