import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import "../../styles/login.css";
import {useDispatch, useSelector} from "react-redux"
import * as AppAction from "../../store/AppActions";

const Login = () => {
    const app = useSelector(state => state.app);
    const [inputName, setUpdateInputName] = useState("");
    const [inputPassword, setUpdatePassword] = useState("");
    const dispatch = useDispatch();
    let history = useHistory();

    function updateName(e) {
        setUpdateInputName(e.target.value);
    }

    function updatePassword(e) {
        setUpdatePassword(e.target.value);
    }

    function sendUserData() {
        dispatch(AppAction.requestLogin(inputName, inputPassword));
    }

    useEffect(() => {
        if (app.isAuth && !app.user.admin) {
            history.push('/chat');
        }
        if (app.user.admin) {
            history.push('/user-list');
        }

    })

    return (
        <div className="login-wrapper">
            <div className="login-structure">
                <div className="login-name">
                    Name
                    <input type="text" onChange={updateName}/>
                </div>
                <div className="login-password">
                    Password
                    <input type="password" onChange={updatePassword}/>
                </div>
                <div className="button-login-wrapper">
                    <button className="send-login-data" id="sendLogin" onClick={sendUserData}>Log in</button>
                </div>
            </div>
        </div>
    )
}
export default Login;