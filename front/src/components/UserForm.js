import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux"
import uuid from 'react-uuid';
import {useHistory} from "react-router-dom";
import "../styles/userForm.css";
import * as userListAction from "../store/userListAction"
import {useParams} from "react-router";


const UserForm = () => {
    const app = useSelector(state => state.app);
    const {id} = useParams();
    const dispatch = useDispatch();
    const [inputUserName, setUpdateInputUserName] = useState("");
    const [inputName, setUpdateInputName] = useState("");
    const [inputPassword, setUpdatePassword] = useState("");

    let history = useHistory();

    function changeUserName(e) {
        setUpdateInputUserName(e.target.value);
    }

    function changeName(e) {
        setUpdateInputName(e.target.value);
    }

    function updatePassword(e) {
        setUpdatePassword(e.target.value);
    }

    function sendUserData() {
        if (!id) {
            const newUser = {
                id: uuid(),
                username: inputUserName,
                password: inputPassword,
                admin: false,
                name: inputName,
                avatar: "https://scontent.fdnk3-2.fna.fbcdn.net/v/t1.0-9/105973293_263889604898479_2423548925171267486_n.jpg?_nc_cat=102&ccb=3&_nc_sid=09cbfe&_nc_ohc=w8GRShVo_mEAX_itICs&_nc_ht=scontent.fdnk3-2.fna&oh=c1049d1854bb3f7f33f2748f1532e4f5&oe=60587BF8",
                updatedAt: new Date().toISOString()
            };
            dispatch(userListAction.switchSpinner());
            dispatch(userListAction.addUsersToList(newUser));
        } else {
            const updateUser = {
                id: id,
                username: inputUserName,
                password: inputPassword,
                name: inputName,
                updatedAt: new Date().toISOString()
            };
            dispatch(userListAction.switchSpinner());
            dispatch(userListAction.updateUser(updateUser));
        }

        closeAddUserForm();
    }

    function closeAddUserForm() {
        history.push('/');
    }

    useEffect(() => {
        if (!app.isAuth) {
            history.push('/');
        }


    }, []);

    return (
        <div className="User-form-wrapper">
            <div className="User-form-block">
                <div className="headerOfForm">Add user</div>
                <div>Username</div>
                <input type="text" onChange={changeUserName}/>
                <div>Name</div>
                <input type="text" onChange={changeName}/>
                <div>Password</div>
                <input type="password" onChange={updatePassword}/>
                <div className="footerOfForm">
                    <input type="button" className="save-button" value="Save" onClick={sendUserData}/>
                    <input type="button" className="cancel-button" value="Cancel" onClick={closeAddUserForm}/>
                </div>
            </div>
        </div>
    )

};


export default (UserForm);