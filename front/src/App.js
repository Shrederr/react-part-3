import './App.css';
import Chat from "./containers/chat";
import Login from "./containers/Login/Login";
import React from 'react';
import {Switch, Route} from "react-router-dom";
import EditModal from "./containers/EditMessage/EditModal";
import UserList from "./containers/UserList/UserList";
import UserForm from "./components/UserForm";


const App = () => {


    return (
        <Switch>
            <Route exact path="/" component={Login}/>
            <Route path="/chat" component={Chat}/>
            <Route path="/editor" component={EditModal}/>
            <Route path="/user-list" component={UserList}/>
            <Route exact path="/user" component={UserForm}/>
            <Route path="/user/:id"><UserForm/></Route>
        </Switch>

    )
}

export default App;



