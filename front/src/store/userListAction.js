export function getUsersList() {
    return {
        type: "GET_USER_LIST"
    }
}

export function setUserList(userList) {
    return {
        type: "SET_USER_LIST",
        userList
    }
}

export function addUsersToList(user) {
    return {
        type: "ADD_USERS_TO_LIST",
        user
    }
}

export function deleteUser(id) {
    return {
        type: "DELETE_USER",
        payload: id
    }

}

export function updateUser(dataToUpdate) {
    return {
        type: "UPDATE_USER",
        payload: dataToUpdate
    }
}

export function switchSpinner() {
    return {
        type: "SWITCH_SPINNER",
        payload: true
    }
}

export function errorGetUserList(message) {
    return {
        type: "ERROR_GET_USER_LIST",
        message
    }
}