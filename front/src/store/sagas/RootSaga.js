import {call, put, takeLeading, all} from "redux-saga/effects";
import axios from "axios";
import {loginError, receiveLogin} from "../AppActions";
import {errorLoading, setMessageList} from "../ChatActions";
import {errorGetUserList, setUserList} from "../userListAction";

const apiUrl = 'http://localhost:3050/api';

function* login(userData) {
    try {
        const response = yield call(axios.post, `${apiUrl}/auth/login`, {
            username: userData.username,
            password: userData.password
        });
        const responseDATA = response.data;
        if (responseDATA.error) {
            yield put(loginError(responseDATA.message));
        }
        yield put(receiveLogin(responseDATA))
    } catch (error) {
        yield put(loginError(error));
    }

}

function* watchLogin() {
    yield takeLeading("LOGIN_REQUEST", login);
}

function* fetchUserList() {
    try {
        const response = yield call(axios.get, `${apiUrl}/users`);
        const responseDATA = response.data;
        if (responseDATA.error) {
            yield put(errorGetUserList(responseDATA.message));
        }
        yield put(setUserList(responseDATA));
    } catch (error) {
        yield put(errorGetUserList(error));
    }
}

function* watchFetchUsers() {
    yield takeLeading("GET_USER_LIST", fetchUserList);
}

function* fetchMessagesList() {
    try {
        const response = yield call(axios.get, `${apiUrl}/message`);
        const responseDATA = response.data;
        if (responseDATA.error) {
            yield put(errorLoading(responseDATA.message))
        }
        yield put(setMessageList(responseDATA));
    } catch (error) {
        yield put(errorLoading(error))
    }
}

function* watchFetchMessages() {
    yield takeLeading("GET_MESSAGES", fetchMessagesList)
}

function* addUserToList(userToAdd) {
    try {
        const response = yield call(axios.post, `${apiUrl}/users`, userToAdd.user);
        const responseDATA = response.data;
        yield put(setUserList(responseDATA));
    } catch (error) {
        console.log(error);
    }
}

function* watchAddUsers() {
    yield takeLeading("ADD_USERS_TO_LIST", addUserToList);
}

function* sendMessage(messageData) {
    try {
        const response = yield call(axios.post, `${apiUrl}/message`, messageData.payload);
        const responseDATA = response.data;
        if (response.error) {
            yield put(errorLoading(responseDATA.message))
        }
        yield put(setMessageList(responseDATA));
    } catch (error) {

    }
}

function* watchSendMessages() {
    yield takeLeading("SEND_MESSAGE", sendMessage);
}

function* deleteUser(id) {
    try {
        const response = yield call(axios.delete, `${apiUrl}/users/${id.payload}`);
        const responseDATA = response.data;
        yield put(setUserList(responseDATA));
    } catch (error) {
        console.log(error);
    }

}

function* watchDeleteUser() {
    yield takeLeading("DELETE_USER", deleteUser);
}

function* deleteMessage(id) {
    try {
        const response = yield call(axios.delete, `${apiUrl}/message/${id.payload}`);
        const responseDATA = response.data;
        yield put(setMessageList(responseDATA));
    } catch (error) {
        console.log(error);
    }
}

function* watchDeleteMessages() {
    yield takeLeading("DELETE_MESSAGE", deleteMessage);
}

function* updateUser(dataToUpdate) {
    try {
        const response = yield call(axios.put, `${apiUrl}/users/${dataToUpdate.payload.id}`,
            {
                username: dataToUpdate.payload.username,
                password: dataToUpdate.payload.password,
                name: dataToUpdate.payload.name,
                updateAt: dataToUpdate.payload.updateAt
            });
        const responseDATA = response.data;
        yield put(setUserList(responseDATA));
    } catch (e) {
        console.log(e);
    }
}

function* watchUpdateUser() {
    yield takeLeading("UPDATE_USER", updateUser);
}

function* editSendMessages(newMessage) {
    const response = yield call(axios.put, `${apiUrl}/message/${newMessage.payload.id}`, {text: newMessage.payload.messageText});
    const responseDATA = response.data;
    yield put(setMessageList(responseDATA));
}

function* watchEditMessages() {
    yield takeLeading("SEND_EDIT_MESSAGE", editSendMessages);
}

export default function* RootSaga() {
    yield all([
        watchLogin(),
        watchFetchMessages(),
        watchFetchUsers(),
        watchAddUsers(),
        watchSendMessages(),
        watchDeleteUser(),
        watchDeleteMessages(),
        watchUpdateUser(),
        watchEditMessages(),
    ])
};