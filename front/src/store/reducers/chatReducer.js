const initialState = {
    messageCounts: 0,
    lastMessageDate: "2020-07-16T19:48:42.481Z",
    messageList: [],
    message: "",
    editMessage: "",
    newEditMessage: "",
    likes: [],
    mutableMessage: {
        isOpen: false,
        id: ""
    },
    spinner: true
}

function sortByDate(arr) {
    arr.sort((a, b) => new Date(a.createdAt).getTime() > new Date(b.createdAt).getTime() ? 1 : -1);
}

export function chatReducer(state = initialState, action) {
    let index = 0;
    switch (action.type) {
        case "SET_MESSAGE_LIST":
            sortByDate(action.messageList);
            return Object.assign({}, state, {
                messageList: action.messageList,
                currentUser: action.messageList[0],
                messageCounts: action.messageList.length,
                lastMessageDate: action.messageList[action.messageList.length - 1].createdAt,
                spinner: false
            });


        case "SET_MESSAGE":
            return {
                ...state,
                message: action.payload
            };
        case "LIKE_MESSAGE":
            index = state.likes.indexOf(action.payload);
            if (index === -1) {
                return {
                    ...state,
                    likes: [...state.likes, action.payload]
                }
            } else {
                const newLikes = [...state.likes];
                newLikes.splice(index, 1);
                return {
                    ...state,
                    likes: newLikes
                }
            }

        case "SWITCH_EDITOR":
            return {
                ...state,
                mutableMessage: {
                    isOpen: action.payload.isOpen,
                    id: action.payload.id
                }
            };


        default:
            return state;
    }

}
