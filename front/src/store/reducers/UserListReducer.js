const initialState = {
    userCounts: 0,
    userList: [],
    spinner: true
};


export function userListReducer(state = initialState, action) {
    switch (action.type) {
        case "SET_USER_LIST":
            return Object.assign({}, state, {
                userList: action.userList,
                userCounts: action.userList.length,
                spinner: false
            });
        case "SWITCH_SPINNER":
            return {
                ...state,
                spinner: action.payload
            };

        default:
            return state;
    }
}