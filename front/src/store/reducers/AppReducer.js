const initialState = {
    isAuth: false,
    error_message: "",
    user: {
        id: "",
        username: "",
        avatar: "",
        admin: false
    }
};

export function appReducer(state = initialState, action) {
    switch (action.type) {
        case "LOGIN_ERROR":
            return {
                ...state,
                isFetching: false,
                isAuth: false,
                error_message: action.message
            };

        case "LOGIN_SUCCESS":

            return {
                isFetching: false,
                isAuth: true,
                user: {
                    id: action.user.id,
                    username: action.user.name,
                    avatar: action.user.avatar,
                    admin: action.user.admin
                }
            };
        default:
            return state;
    }
}