import {chatReducer} from "./chatReducer";
import {combineReducers} from "redux";
import {appReducer} from "./AppReducer";
import {userListReducer} from "./UserListReducer";

const RootReducer = combineReducers({
    chat: chatReducer,
    app: appReducer,
    userList: userListReducer
});

export default RootReducer;

