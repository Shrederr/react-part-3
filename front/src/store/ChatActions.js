export function getMessages() {
    return {
        type: "GET_MESSAGES"
    }
}

export function setMessageList(messageList) {
    return {
        type: "SET_MESSAGE_LIST",
        messageList
    }
}

export function errorLoading() {
    return {
        type: "ERROR_LOADING_MESSAGE_LIST"
    }
}

export function sendMessage(newMessage) {
    return {
        type: "SEND_MESSAGE",
        payload: newMessage
    }
}

export function setMessage(message) {
    return {
        type: "SET_MESSAGE",
        payload: message
    }
}

export function likeMessage(id) {
    return {
        type: "LIKE_MESSAGE",
        payload: id
    }
}

export function deleteMessage(id) {
    return {
        type: "DELETE_MESSAGE",
        payload: id
    }
}

export function switchEditor(status) {
    return {
        type: "SWITCH_EDITOR",
        payload: status
    }
}

export function sendEditMessage(newMessage, id) {
    return {
        type: "SEND_EDIT_MESSAGE",
        payload: {
            messageText: newMessage,
            id: id
        }
    }
}

