import {createStore, applyMiddleware, Store} from 'redux';
import RootReducer from "./reducers/RootReducer";
import createSagaMiddleware from 'redux-saga';
import RootSaga from "./sagas/RootSaga";


const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    RootReducer,
    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(RootSaga);

export default store;