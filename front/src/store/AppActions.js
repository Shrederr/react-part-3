export function loginError(message) {
    return {
        type: "LOGIN_ERROR",
        message
    }
}

export function requestLogin(username, password) {
    return {
        type: "LOGIN_REQUEST",
        username,
        password
    }
}

export function receiveLogin(user) {
    return {
        type: "LOGIN_SUCCESS",
        isFetching: false,
        isAuth: true,
        user
    }
}